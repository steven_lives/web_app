import React, { Component } from "react";
import Home from './components/Home.js'
import User from './components/User.js'
import Login from './components/Login.js'
import { HashRouter, Route, Link } from 'react-router-dom'

class App extends Component {
    render() {
        return (
            <div className="App">
                <HashRouter>
                    <ul>
                        <li><Link to='/home'>首页</Link></li>
                        <li><Link to='/user'>用户</Link></li>
                        <li><Link to='/login'>登录</Link></li>
                    </ul>
                    <Switch>
                        <Route path='/home' component={Home} />
                        <Route path='/user' component={User} />
                        <Route path='/login' component={Login} />
                    </Switch>
                </HashRouter>
            </div>
        )
    }
}
export default App